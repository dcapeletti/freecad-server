# FreeCAD-Server

# Objetivos
Comunicación bidireccional en tiempo real entre estados del diseño 3D y estados del hardware

Real-time bidirectional communication between 3D design states and hardware states

El modelo (Led) tiene una propiedad Estado (State) que cambia en función del valor que recibe de los clientes. Pero también el usuario de FreeCAD puede cambiar ese valor y FreeCAD reporta a los clientes conectados. Cualquier cambio en los valores de estado (en el hardware o en el modelo), pueden ser reflejados o no en la vista 3D del modelo.
Esto funciona así gracias 2 patrones claves, un observador que observa el estado del modelo en el diseño (FreeCAD) y tecnología de comunicación cliente-servidor (por medio de sockets) que permiten operaciones (CRUD) en FreeCAD. Cuando el usuario FreeCAD cambia la propiedad State en el modelo, un observador informa al servidor sobre ese cambio y los clientes conectados al servidor FreeCAD son informados para cambiar el estado del hardware.

¿Qué sentido de aplicaciones tiene esto?
Bueno, este es un ejemplo muy sencillo, un Led prácticamente es inútil para hacer algo. Pero esto puede ser útil para control industrial, domótica, robótica, dónde la necesidad del estado diseño 3D -<-> Hardware tiene sentido en la práctica. Y ¿qué tal si hacemos un workbench Servidor con Observador sobre modelos?
Ver video https://www.youtube.com/watch?v=Ta3nfMsbhDE

# English
The model (Led) has a State property that changes depending on the value it receives from the clients. But also the FreeCAD user can change that value and FreeCAD reports to the connected clients. Any change in the state values (in the hardware or in the model), can be reflected or not in the 3D view of the model.
This works this way thanks to 2 key patterns, an observer that observes the state of the model in the design (FreeCAD) and client-server communication technology (via sockets) that allows operations (CRUD) in FreeCAD. When the FreeCAD user changes the State property in the model, an observer informs the server about that change and clients connected to the FreeCAD server are informed to change the hardware state.

What is the application sense of this?
Well, this is a very simple example, an LED is practically useless for doing anything. But this can be useful for industrial control, home automation, robotics, where the need for 3D design -<-> Hardware state makes practical sense. And what if we make a workbench Server with Observer on models?

See video https://www.youtube.com/watch?v=Ta3nfMsbhDE

